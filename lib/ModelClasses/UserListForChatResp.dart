// To parse this JSON data, do
//
//     final userListForChatResp = userListForChatRespFromJson(jsonString);

import 'dart:convert';

UserListForChatResp userListForChatRespFromJson(String str) => UserListForChatResp.fromJson(json.decode(str));

String userListForChatRespToJson(UserListForChatResp data) => json.encode(data.toJson());

class UserListForChatResp {
  UserListForChatResp({
    this.statusCode,
    this.message,
    this.error,
    this.data,
  });

  int statusCode;
  String message;
  bool error;
  List<UserListForChatModel> data;

  factory UserListForChatResp.fromJson(Map<String, dynamic> json) => UserListForChatResp(
    statusCode: json["status_code"],
    message: json["message"],
    error: json["error"],
    data: List<UserListForChatModel>.from(json["data"].map((x) => UserListForChatModel.fromJson(x))),
  );

  Map<String, dynamic> toJson() => {
    "status_code": statusCode,
    "message": message,
    "error": error,
    "data": List<dynamic>.from(data.map((x) => x.toJson())),
  };
}

class UserListForChatModel {
  UserListForChatModel({
    this.userId,
    this.sessionId,
    this.firstName,
    this.lastName,
    this.email,
    this.phone,
    this.dob,
    this.city,
    this.country,
    this.address,
    this.likes,
    this.onlineStatus,
    this.profileImages,
    this.aboutMe,
    this.latitude,
    this.longitude,
    this.interest,
    this.gender,
    this.education,
    this.profession,
    this.schoolCollege,
    this.facebookId,
    this.googleId,
    this.twitterId,
    this.otp,
  });

  int userId;
  String sessionId;
  String firstName;
  String lastName;
  String email;
  String phone;
  String dob;
  String city;
  String country;
  String address;
  String likes;
  String onlineStatus;
  String profileImages;
  String aboutMe;
  String latitude;
  String longitude;
  String interest;
  String gender;
  String education;
  String profession;
  String schoolCollege;
  String facebookId;
  String googleId;
  String twitterId;
  String otp;

  factory UserListForChatModel.fromJson(Map<String, dynamic> json) => UserListForChatModel(
    userId: json["user_id"],
    sessionId: json["session_id"],
    firstName: json["first_name"],
    lastName: json["last_name"],
    email: json["email"],
    phone: json["phone"],
    dob: json["dob"],
    city: json["city"],
    country: json["country"],
    address: json["address"],
    likes: json["likes"],
    onlineStatus: json["online_status"],
    profileImages: json["profile_images"],
    aboutMe: json["about_me"],
    latitude: json["latitude"],
    longitude: json["longitude"],
    interest: json["interest"],
    gender: json["gender"],
    education: json["education"],
    profession: json["profession"],
    schoolCollege: json["school_college"],
    facebookId: json["facebook_id"],
    googleId: json["google_id"],
    twitterId: json["twitter_id"],
    otp: json["otp"],
  );

  Map<String, dynamic> toJson() => {
    "user_id": userId,
    "session_id": sessionId,
    "first_name": firstName,
    "last_name": lastName,
    "email": email,
    "phone": phone,
    "dob": dob,
    "city": city,
    "country": country,
    "address": address,
    "likes": likes,
    "online_status": onlineStatus,
    "profile_images": profileImages,
    "about_me": aboutMe,
    "latitude": latitude,
    "longitude": longitude,
    "interest": interest,
    "gender": gender,
    "education": education,
    "profession": profession,
    "school_college": schoolCollege,
    "facebook_id": facebookId,
    "google_id": googleId,
    "twitter_id": twitterId,
    "otp": otp,
  };
}
