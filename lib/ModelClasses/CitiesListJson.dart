import 'dart:convert';

CitiesListJson citiesListJsonFromJson(String str) => CitiesListJson.fromJson(json.decode(str));

String citiesListJsonToJson(CitiesListJson data) => json.encode(data.toJson());

class CitiesListJson {
  CitiesListJson({
    this.statusCode,
    this.message,
    this.statesCities,
  });

  int statusCode;
  String message;
  List<StatesCity> statesCities;

  factory CitiesListJson.fromJson(Map<String, dynamic> json) => CitiesListJson(
    statusCode: json["status_code"],
    message: json["message"],
    statesCities: List<StatesCity>.from(json["StatesCities"].map((x) => StatesCity.fromJson(x))),
  );

  Map<String, dynamic> toJson() => {
    "status_code": statusCode,
    "message": message,
    "StatesCities": List<dynamic>.from(statesCities.map((x) => x.toJson())),
  };
}

class StatesCity {
  StatesCity({
    this.id,
    this.name,
    this.countryId,
    this.cities,
  });

  int id;
  String name;
  int countryId;
  List<City> cities;

  factory StatesCity.fromJson(Map<String, dynamic> json) => StatesCity(
    id: json["id"],
    name: json["name"],
    countryId: json["country_id"],
    cities: List<City>.from(json["cities"].map((x) => City.fromJson(x))),
  );

  Map<String, dynamic> toJson() => {
    "id": id,
    "name": name,
    "country_id": countryId,
    "cities": List<dynamic>.from(cities.map((x) => x.toJson())),
  };
}

class City {
  City({
    this.id,
    this.name,
    this.stateId,
  });

  int id;
  String name;
  int stateId;

  factory City.fromJson(Map<String, dynamic> json) => City(
    id: json["id"],
    name: json["name"],
    stateId: json["state_id"],
  );

  Map<String, dynamic> toJson() => {
    "id": id,
    "name": name,
    "state_id": stateId,
  };
}
