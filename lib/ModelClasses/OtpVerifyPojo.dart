
import 'dart:convert';

OtpVerifyPojo otpVerifyPojoFromJson(String str) => OtpVerifyPojo.fromJson(json.decode(str));

String otpVerifyPojoToJson(OtpVerifyPojo data) => json.encode(data.toJson());

class OtpVerifyPojo {
  OtpVerifyPojo({
    this.statusCode,
    this.message,
    this.error,
    this.errorMessage,
    this.data,
    this.toCheck,
  });

  int statusCode;
  String message;
  bool error;
  String errorMessage;
  Data data;
  bool toCheck;

  factory OtpVerifyPojo.fromJson(Map<String, dynamic> json) => OtpVerifyPojo(
    statusCode: json["status_code"],
    message: json["message"],
    error: json["error"],
    errorMessage: json["error_message"],
    data: Data.fromJson(json["data"]),
    toCheck: json["to_check"],
  );

  Map<String, dynamic> toJson() => {
    "status_code": statusCode,
    "message": message,
    "error": error,
    "error_message": errorMessage,
    "data": data.toJson(),
    "to_check": toCheck,
  };
}

class Data {
  Data({
    this.userId,
    this.sessionId,
    this.firstName,
    this.lastName,
    this.phone,
    this.dob,
    this.city,
    this.country,
    this.address,
    this.likes,
    this.onlineStatus,
    this.profileImages,
    this.aboutMe,
    this.latitude,
    this.longitude,
    this.interest,
    this.gender,
    this.education,
    this.profession,
    this.schoolCollege,
    this.facebookId,
    this.googleId,
    this.twitterId,
    this.otp,
  });

  int userId;
  String sessionId;
  String firstName;
  String lastName;
  String phone;
  DateTime dob;
  String city;
  String country;
  String address;
  String likes;
  String onlineStatus;
  String profileImages;
  String aboutMe;
  String latitude;
  String longitude;
  String interest;
  String gender;
  String education;
  String profession;
  String schoolCollege;
  String facebookId;
  String googleId;
  String twitterId;
  String otp;

  factory Data.fromJson(Map<String, dynamic> json) => Data(
    userId: json["user_id"],
    sessionId: json["session_id"],
    firstName: json["first_name"],
    lastName: json["last_name"],
    phone: json["phone"],
    dob: DateTime.parse(json["dob"]),
    city: json["city"],
    country: json["country"],
    address: json["address"],
    likes: json["likes"],
    onlineStatus: json["online_status"],
    profileImages: json["profile_images"],
    aboutMe: json["about_me"],
    latitude: json["latitude"],
    longitude: json["longitude"],
    interest: json["interest"],
    gender: json["gender"],
    education: json["education"],
    profession: json["profession"],
    schoolCollege: json["school_college"],
    facebookId: json["facebook_id"],
    googleId: json["google_id"],
    twitterId: json["twitter_id"],
    otp: json["otp"],
  );

  Map<String, dynamic> toJson() => {
    "user_id": userId,
    "session_id": sessionId,
    "first_name": firstName,
    "last_name": lastName,
    "phone": phone,
    "dob": "${dob.year.toString().padLeft(4, '0')}-${dob.month.toString().padLeft(2, '0')}-${dob.day.toString().padLeft(2, '0')}",
    "city": city,
    "country": country,
    "address": address,
    "likes": likes,
    "online_status": onlineStatus,
    "profile_images": profileImages,
    "about_me": aboutMe,
    "latitude": latitude,
    "longitude": longitude,
    "interest": interest,
    "gender": gender,
    "education": education,
    "profession": profession,
    "school_college": schoolCollege,
    "facebook_id": facebookId,
    "google_id": googleId,
    "twitter_id": twitterId,
    "otp": otp,
  };
}
