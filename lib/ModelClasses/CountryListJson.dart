// To parse this JSON data, do
//
//     final countryListJson = countryListJsonFromJson(jsonString);
import 'dart:convert';

CountryListJson countryListJsonFromJson(String str) => CountryListJson.fromJson(json.decode(str));

String countryListJsonToJson(CountryListJson data) => json.encode(data.toJson());

class CountryListJson {
  CountryListJson({
    this.statusCode,
    this.message,
    this.countriesList,
  });

  int statusCode;
  String message;
  List<CountriesList> countriesList;

  factory CountryListJson.fromJson(Map<String, dynamic> json) => CountryListJson(
    statusCode: json["status_code"],
    message: json["message"],
    countriesList: List<CountriesList>.from(json["countriesList"].map((x) => CountriesList.fromJson(x))),
  );

  Map<String, dynamic> toJson() => {
    "status_code": statusCode,
    "message": message,
    "countriesList": List<dynamic>.from(countriesList.map((x) => x.toJson())),
  };
}

class CountriesList {
  CountriesList({
    this.id,
    this.sortname,
    this.name,
    this.phonecode,
  });

  int id;
  String sortname;
  String name;
  int phonecode;

  factory CountriesList.fromJson(Map<String, dynamic> json) => CountriesList(
    id: json["id"],
    sortname: json["sortname"],
    name: json["name"],
    phonecode: json["phonecode"],
  );

  Map<String, dynamic> toJson() => {
    "id": id,
    "sortname": sortname,
    "name": name,
    "phonecode": phonecode,
  };
}