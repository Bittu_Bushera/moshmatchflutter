import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

class AbhayChatPage extends StatefulWidget {
  @override
  _AbhayChatPageState createState() => _AbhayChatPageState();
}

class _AbhayChatPageState extends State<AbhayChatPage> {
  var _Abhay = "";

  @override
  Widget build(BuildContext context) {
    var valPaddingLeft = MediaQuery.of(context).size.width * 0.05;
    var valPaddingRight = MediaQuery.of(context).size.width * 0.05;
    return SafeArea(
      child: Scaffold(
        backgroundColor: Colors.indigo,
        body: Container(
          width: MediaQuery.of(context).size.width,
          height: MediaQuery.of(context).size.height,
          child: Column(
            children: <Widget>[
              Container(
                width: MediaQuery.of(context).size.width,
                height: MediaQuery.of(context).size.height * 0.2,
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: <Widget>[
                    Padding(
                      padding: const EdgeInsets.all(8.0),
                      child: IconButton(
                          icon: Icon(
                            Icons.arrow_back_ios,
                            color: Colors.white,
                          ),
                          onPressed: () {}),
                    ),
                    Stack(
                      children: <Widget>[
                        Align(
                          alignment: Alignment.topLeft,
                          child: Container(
                            width: 55,
                            height: 55,
                            decoration: BoxDecoration(
                                shape: BoxShape.circle,
                                image: DecorationImage(
                                    image:
                                        AssetImage('assets/images/splash.jpg'),
                                    fit: BoxFit.fill)),
                          ),
                        ),
                        Align(
                          alignment: Alignment(0, -1),
                          child: Column(
                            crossAxisAlignment: CrossAxisAlignment.start,
                            children: <Widget>[
                              Text(
                                'ABhay Kumar',
                                style: TextStyle(
                                    fontWeight: FontWeight.bold,
                                    color: Colors.white,
                                    fontSize: 18.0),
                              ),
                              Text(
                                'ABhay Kumar',
                                style: TextStyle(
                                    fontWeight: FontWeight.bold,
                                    color: Colors.white,
                                    fontSize: 18.0),
                              )
                            ],
                          ),
                        ),
                        Align(
                          alignment: Alignment.topRight,
                          child: Container(
                            width: 45,
                            height: 45,
                            decoration: BoxDecoration(
                                shape: BoxShape.circle,
                                image: DecorationImage(
                                    image:
                                        AssetImage('assets/images/splash.jpg'),
                                    fit: BoxFit.fill)),
                          ),
                        )
                      ],
                    ),

                    /*       Row(
                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      children: <Widget>[
                        Container(
                          width: 55,
                          height: 55,
                          decoration: BoxDecoration(
                              shape: BoxShape.circle,
                              image: DecorationImage(
                                  image:
                                      AssetImage('assets/images/splash.jpg'))),
                        ),
                        Column(
                          children: <Widget>[
                            Text(
                              'ABhay Kumar',
                              style: TextStyle(
                                  fontWeight: FontWeight.bold,
                                  color: Colors.white,
                                  fontSize: 18.0),
                            ),
                            Text(
                              'ABhay Kumar',
                              style: TextStyle(
                                  fontWeight: FontWeight.bold,
                                  color: Colors.white,
                                  fontSize: 18.0),
                            )
                          ],
                        ),
                        Container(
                          width: 45,
                          height: 45,
                          decoration: BoxDecoration(
                              shape: BoxShape.circle,
                              image: DecorationImage(
                                  image:
                                      AssetImage('assets/images/splash.jpg'))),
                        )
                      ],
                    )*/
                  ],
                ),
              ),
              chatlist(),
              Container(
                alignment: Alignment.bottomCenter,
                child: Column(
                  children: <Widget>[
                    Container(
                      alignment: Alignment.bottomCenter,
                      height: MediaQuery.of(context).size.height * 0.1,
                      child: Row(
                        children: <Widget>[
                          Container(
                            padding: EdgeInsets.only(
                                right: valPaddingRight, left: valPaddingLeft),
//                margin: EdgeInsets.only(top: 30),

                            width: MediaQuery.of(context).size.width * 0.8,
                            child: TextField(
                              //  controller: msgEditingController,
                              decoration: InputDecoration(
                                border: OutlineInputBorder(
                                    borderRadius: BorderRadius.all(
                                        Radius.circular(50.0))),
                                labelText: 'Enter message here',
                                labelStyle: TextStyle(color: Colors.amber),
                                hintStyle: TextStyle(color: Colors.grey),
                                fillColor: Color.fromRGBO(255, 255, 255, 0.5),
                                filled: true,
                              ),
                            ),
                          ),
                          Container(
                            width: MediaQuery.of(context).size.width * 0.2,
                            child: InkWell(
                              child: Text(
                                'Send',
                                textAlign: TextAlign.center,
                                style: TextStyle(color: Colors.amber),
                              ),
                              onTap: () {
                                //chkFieldsValues(context);
                              },
                            ),
                          ),
                          /* Container(
                              width: MediaQuery.of(context).size.width * 0.1,
                              child: Text("sdfs"),
                            )*/
                        ],
                      ),
                    ),
                    SizedBox(
                      height: MediaQuery.of(context).size.height * 0.05,
                    )
                  ],
                ),
              )
            ],
          ),
        ),
      ),
    );
  }

  Widget chatlist() {
    return SizedBox(
        width: MediaQuery.of(context).size.width,
        height: MediaQuery.of(context).size.height * 0.5,
        child: ListView.builder(
            itemCount: 10,
            itemBuilder: (context, position) {
              if (position % 2 == 0) {
                Text(
                  "sjdklfjdsf",
                  textAlign: TextAlign.start,
                  style: TextStyle(color: Colors.white, fontSize: 18.0),
                );
              } else {
                Text(
                  "sjdklfjdsf",
                  textAlign: TextAlign.end,
                );
              }
            }));
  }
}
