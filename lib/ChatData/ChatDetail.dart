import 'dart:async';
import 'dart:convert';

import 'package:adhara_socket_io/adhara_socket_io.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:mosh_match/ChatData/AllChatMessagesRes.dart';
import 'package:mosh_match/ChatData/SingleChatMessagesRes.dart';
import 'package:mosh_match/ModelClasses/UserListForChatResp.dart';
import 'package:shared_preferences/shared_preferences.dart';

UserListForChatModel holdUserData = UserListForChatModel();
const String URI =
    "http://167.172.209.57:4000/"; //http://3.18.215.33:3000/     10.0.0.2

class ChatDetail extends StatefulWidget {
  ChatDetail(gotData) {
    holdUserData = gotData;
  }

  @override
  State<StatefulWidget> createState() {
    return _ChatDetailState();
  }
}

class _ChatDetailState extends State<ChatDetail> {
  final msgEditingController = TextEditingController();
  List<String> toPrint = ["trying to connect"];
  SocketIOManager manager = SocketIOManager();
  SocketIO socket;
  String TAG = "ChatDetail ";
  bool isSocketConnected = false;
  String strUserForChatApiResponse = "";
  List<SingleChatMessagesRes> _messageList = List<SingleChatMessagesRes>();

  Map<String, SocketIO> sockets = {};
  var _scaffoldKey = GlobalKey<ScaffoldState>();
  var aaa = "";
  String strSessionId = "";

  @override
  void initState() {
    super.initState();

    manager = SocketIOManager();
    // chkFieldsValues(context);
    startSocketChat(context);
  }


  @override
  Widget build(BuildContext context) {
    var valPaddingLeft = MediaQuery
        .of(context)
        .size
        .width * 0.05;
    var valPaddingRight = MediaQuery
        .of(context)
        .size
        .width * 0.05;
    debugPrint(
        "dddddddddddddddddddddddddddddddddddddddddddddddddddddddddddddddddddd");
    return SafeArea(
        child: Scaffold(
            key: _scaffoldKey,
            body: Stack(
              children: <Widget>[
                Container(
                  width: MediaQuery
                      .of(context)
                      .size
                      .width,
                  height: MediaQuery
                      .of(context)
                      .size
                      .height,
                  constraints: BoxConstraints.expand(),
                  decoration: BoxDecoration(
                      image: DecorationImage(
                          image: AssetImage("assets/images/bg.jpg"),
                          fit: BoxFit.cover)),
                ),
                ListView(
                  shrinkWrap: true,
                  children: <Widget>[
                    Container(
                      height: MediaQuery
                          .of(context)
                          .size
                          .height * 0.1,
                      child: Text(
                        aaa +
                            holdUserData.firstName +
                            " " +
                            holdUserData.lastName,
                        textAlign: TextAlign.center,
                        style: TextStyle(color: Colors.amber),
                      ),
                    ),
                    Container(
                      height: MediaQuery
                          .of(context)
                          .size
                          .height * 0.65,
                      //child: Text("top chat page")
                      child: ListView.builder(
                        shrinkWrap: true,
                        itemCount: _messageList.length,
                        itemBuilder: _buildItemsForListView,
                      ),
                    ),
                    Container(
                      alignment: Alignment.bottomCenter,
                      height: MediaQuery
                          .of(context)
                          .size
                          .height * 0.1,
                      child: Row(
                        children: <Widget>[
                          Container(
                            padding: EdgeInsets.only(
                                right: valPaddingRight, left: valPaddingLeft),
//                margin: EdgeInsets.only(top: 30),

                            width: MediaQuery
                                .of(context)
                                .size
                                .width * 0.8,
                            child: TextField(
                              controller: msgEditingController,
                              decoration: InputDecoration(
                                border: OutlineInputBorder(
                                    borderRadius: BorderRadius.all(
                                        Radius.circular(50.0))),
                                labelText: 'Enter message here',
                                labelStyle: TextStyle(color: Colors.amber),
                                hintStyle: TextStyle(color: Colors.grey),
                                fillColor: Color.fromRGBO(255, 255, 255, 0.5),
                                filled: true,
                              ),
                            ),
                          ),
                          Container(
                            width: MediaQuery
                                .of(context)
                                .size
                                .width * 0.2,
                            child: InkWell(
                              child: Text(
                                'Send',
                                textAlign: TextAlign.center,
                                style: TextStyle(color: Colors.amber),
                              ),
                              onTap: () {
                                chkFieldsValues(context);
                              },
                            ),
                          ),
                          /* Container(
                              width: MediaQuery.of(context).size.width * 0.1,
                              child: Text("sdfs"),
                            )*/
                        ],
                      ),
                    ),
                    SizedBox(
                      height: MediaQuery
                          .of(context)
                          .size
                          .height * 0.05,
                    )
                  ],
                )
              ],
            )));
  }

  ListTile _buildItemsForListView(BuildContext context, int index) {
    return ListTile(
      leading: Icon(
        Icons.star,
        color: Colors.white,
      ),
      title: Text(_messageList[index].message + "dddddddddd",
          style: TextStyle(fontSize: 12, color: Colors.white)),
      /*    subtitle: Text(_messageList[index].phone,
          style: TextStyle(fontSize: 12, color: Colors.white))*/
      onTap: () {
        /* Navigator.of(context).push(
        MaterialPageRoute(builder: (context) => Settings()),*/
      },
    );
  }

/*todo----list of msg
*  todo----send krna hai
*   todo-----send msg list me add krna hai
*    */

  void chkFieldsValues(BuildContext context) async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    strSessionId = prefs.getString('sessionId');
    print(TAG + " " + strSessionId);

    if (isSocketConnected) {
      if (msgEditingController.text
          .toString()
          .replaceAll(" ", "")
          .isEmpty) {
        _showSnackBar(context, "Enter a message");
        debugPrint("field is empty");
      } else {


        var responseData = [
          {
            "session_id": strSessionId,
            "reciever_id": holdUserData.userId,
            "message": "${msgEditingController.text
                .toString()} from android team " +
                DateTime.now().toString(),
            "media": ""
          }
        ];

        debugPrint("inputData=" + responseData.toString());
        socket.emit('send_message', responseData);

        socket.on('getMessage', (data) {
          // hera  is where you receive from `server`
          debugPrint('getMessage');
          debugPrint("getMessage listened=" + data.toString());
        });

        socket.on("message_data1", (data) {
          msgEditingController.text = "";
          debugPrint('message_data1');
          debugPrint("message_data1 listened=" + data.toString());
        });
      }
    }
    else {
      startSocketChat(context);
    }
/*
      socket.onConnect((data) {
        debugPrint("connected...");
        debugPrint("onConnectData= " + data.toString());


        socket.on("getMessageNew", (data) {
          debugPrint("getMessageNew listened data= " + data);
        });
      });

      socket.connect();
*/
    debugPrint("socket.isConnected()= ${socket.isConnected()} ");
  }


  _showSnackBar(BuildContext context, var a) {
    final snackBar = SnackBar(content: Text(a));
    _scaffoldKey.currentState.showSnackBar(snackBar);
  }

  @override
  void dispose() {
    // TODO: implement dispose

    super.dispose();
  }

  void startSocketChat(BuildContext context) async {
    socket = await manager.createInstance(SocketOptions(URI));
    SharedPreferences prefs = await SharedPreferences.getInstance();
    strSessionId = prefs.getString('sessionId');
    debugPrint(TAG + " " + strSessionId);

    socket.onConnect((data) {
      isSocketConnected = true;
      debugPrint("connected...");
      debugPrint("onConnectData= " + data.toString());

      var responseData = [
        {
          "session_id": strSessionId,
          "reciever_id": holdUserData.userId,
        }
      ];

      debugPrint("inputData for authenticate=" + responseData.toString());
      this.socket.emit('authenticate', responseData);

      socket.on('getMessage', (data) {
        // hera  is where you receive from `server`
        debugPrint("getMessage listened=" + data.toString());

        Timer(Duration(seconds: 3), () {
          setDataInLst(data.toString());
        });
      });
    });

if(!isSocketConnected)
  {socket.connect();
  }

    debugPrint(
        "startSocketChat socket.isConnected()= ${socket.isConnected()} ");
  }

  void setDataInLst(String strMsgData) {

   setState(() {
     aaa =strMsgData;
   //  var a= allChatMessagesResFromJson(strMsgData);

    // debugPrint("ABHAY ABHA"+a.toString());
     var b=jsonEncode(aaa);
   //  debugPrint("ABHAY ABHAY ABHAY AB"+b);
     var c=jsonDecode(b);
     debugPrint("ABHAY ABHAY ABHAY AB"+c.toJson());
   });

    String allChatResp =strMsgData;
    List<AllChatMessagesRes> allMsgLst =
    allChatMessagesResFromJson(allChatResp);
    List<SingleChatMessagesRes> _messageListTemp = List<SingleChatMessagesRes>();

    debugPrint("setDataInLst allMsgLst.length= ${allMsgLst.length}"  );

    for (int i = 0; i < allMsgLst.length; i++) {
      debugPrint("outer ttter " + allMsgLst.length.toString());

      for (int j = 0; j < allMsgLst[i].data.length; j++) {
        SingleChatMessagesRes singleChatMessagesRes =
        SingleChatMessagesRes();

    /*    singleChatMessagesRes.senderId =
            allMsgLst[i].data[j].userFromId;
        singleChatMessagesRes.message = allMsgLst[i].data[j].message;
        singleChatMessagesRes.recieverId =
            int.parse(allMsgLst[i].data[j].userToId);

        _messageListTemp.add(singleChatMessagesRes);*/
        debugPrint(
            "userFromId=${allMsgLst[i].data[j].userFromId}   message=${allMsgLst[i].data[j].message}      userToId=${allMsgLst[i].data[j].userToId}   ");

        debugPrint(
            "dddadfffffffffffffffffffdfgdsgdgdfgdfgsdf ${_messageList.length}");
      }
    }

    setState(() {
      _messageList=_messageListTemp;
      debugPrint(
          TAG+"_messageList.length ${_messageList.length}");
    });

    //_messageList
    debugPrint(" sigetMessageze=   ${_messageList.length}");

  }
}
//https://prnt.sc/vekz9t
