import 'dart:convert';

List<SingleChatMessagesRes> singleChatMessagesResFromJson(String str) => List<SingleChatMessagesRes>.from(json.decode(str).map((x) => SingleChatMessagesRes.fromJson(x)));

String singleChatMessagesResToJson(List<SingleChatMessagesRes> data) => json.encode(List<dynamic>.from(data.map((x) => x.toJson())));

class SingleChatMessagesRes {
  SingleChatMessagesRes({
    this.sessionId,
    this.recieverId,
    this.senderId,
    this.message,
    this.media,
  });

  String sessionId;
  String senderId;
  int recieverId;
  String message;
  String media;

  factory SingleChatMessagesRes.fromJson(Map<String, dynamic> json) => SingleChatMessagesRes(
    sessionId: json["session_id"],
    recieverId: json["reciever_id"],
    message: json["message"],
    media: json["media"],
  );

  Map<String, dynamic> toJson() => {
    "session_id": sessionId,
    "reciever_id": recieverId,
    "message": message,
    "media": media,
  };
}
