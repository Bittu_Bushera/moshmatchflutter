// To parse this JSON data, do
//
//     final allChatMessagesRes = allChatMessagesResFromJson(jsonString);

import 'dart:convert';

List<AllChatMessagesRes> allChatMessagesResFromJson(String str) => List<AllChatMessagesRes>.from(json.decode(str).map((x) => AllChatMessagesRes.fromJson(x)));

String allChatMessagesResToJson(List<AllChatMessagesRes> data) => json.encode(List<dynamic>.from(data.map((x) => x.toJson())));

class AllChatMessagesRes {
  AllChatMessagesRes({
    this.title,
    this.data,
  });

  String title;
  List<Datum> data;

  factory AllChatMessagesRes.fromJson(Map<String, dynamic> json) => AllChatMessagesRes(
    title: json["title"],
    data: List<Datum>.from(json["data"].map((x) => Datum.fromJson(x))),
  );

  Map<String, dynamic> toJson() => {
    "title": title,
    "data": List<dynamic>.from(data.map((x) => x.toJson())),
  };
}

class Datum {
  Datum({
    this.id,
    this.userToId,
    this.userFromId,
    this.message,
    this.createdAt,
    this.updatedAt,
    this.media,
    this.messageIsRead,
  });

  String id;
  String userToId;
  String userFromId;
  String message;
  DateTime createdAt;
  DateTime updatedAt;
  String media;
  String messageIsRead;

  factory Datum.fromJson(Map<String, dynamic> json) => Datum(
    id: json["id"],
    userToId: json["user_to_id"],
    userFromId: json["user_from_id"],
    message: json["message"],
    createdAt: DateTime.parse(json["created_at"]),
    updatedAt: DateTime.parse(json["updated_at"]),
    media: json["media"],
    messageIsRead: json["message_is_read"],
  );

  Map<String, dynamic> toJson() => {
    "id": id,
    "user_to_id": userToId,
    "user_from_id": userFromId,
    "message": message,
    "created_at": createdAt.toIso8601String(),
    "updated_at": updatedAt.toIso8601String(),
    "media": media,
    "message_is_read": messageIsRead,
  };
}
