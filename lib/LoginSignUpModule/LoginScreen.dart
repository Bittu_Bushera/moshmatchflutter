import 'dart:convert';

import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_facebook_login/flutter_facebook_login.dart';
import 'package:mosh_match/GlobalDataUtils.dart';
import 'file:///D:/Android%20Projects/Office%20Projects/New%20Projects/Flutter/MoshMatch%20all%20things/mosh_match/lib/HomeData/HomeScreen.dart';
import 'package:mosh_match/LoginSignUpModule/SignupScreen.dart';
import 'package:mosh_match/ModelClasses/LoginPojo.dart';
import 'package:mosh_match/StorageUtil.dart';
import 'package:mosh_match/webApiCall.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:http/http.dart' as http;
import 'dart:convert' as JSON;

class LoginScreen extends StatefulWidget {
  @override
  State<StatefulWidget> createState() {
    return _LoginScreen();
  }
}

class _LoginScreen extends State<LoginScreen> {
  var _scaffoldKey = GlobalKey<ScaffoldState>();
  final emailEditingController = TextEditingController();
  final psdEditingController = TextEditingController();
  LoginPojo loginApiResponse;
  bool isLoadingApi = false;
  String strLoginApiResponse = "";
  bool _isFBLoggedIn = false;
  Map userFaceBookProfile;
  final facebookLogin = FacebookLogin();

  @override
  Widget build(BuildContext context) {
    var checkedValue = false;
    var valPaddingLeft = MediaQuery.of(context).size.width * 0.05;
    var valPaddingRight = MediaQuery.of(context).size.width * 0.05;

    return MaterialApp(
        theme: ThemeData(
          brightness: Brightness.dark,
          primaryColor: Colors.orange,
          accentColor: Colors.amber,
          // Define the default font family.
          fontFamily: 'Georgia',
          // Define the default TextTheme. Use this to specify the default
          // text styling for headlines, titles, bodies of text, and more.
          textTheme: TextTheme(
            headline1: TextStyle(fontSize: 72.0, fontWeight: FontWeight.bold),
            headline6: TextStyle(fontSize: 36.0, fontStyle: FontStyle.italic),
            bodyText2: TextStyle(fontSize: 14.0, fontFamily: 'Hind'),
          ),
        ),
        debugShowCheckedModeBanner: false,
        title: "Mosh Match",
        home: SafeArea(
            child: Scaffold(
                key: _scaffoldKey,
                body: Container(
                  constraints: BoxConstraints.expand(),
                  decoration: BoxDecoration(
                      image: DecorationImage(
                          image: AssetImage("assets/images/bg.jpg"),
                          fit: BoxFit.cover)),
                  child: Expanded(
                    child: ListView(
                      children: <Widget>[
                        SizedBox(
                          height: MediaQuery.of(context).size.height * 0.05,
                        ),
                        Image(
                          image: AssetImage('assets/images/logo.png'),
                          height: MediaQuery.of(context).size.height * 0.2,
                        ),
                        SizedBox(
                          height: MediaQuery.of(context).size.height * 0.02,
                        ),
                        Container(
                          alignment: Alignment.center,
                          //margin: EdgeInsets.only(top: 30),
                          padding: EdgeInsets.only(
                              right: valPaddingRight, left: valPaddingLeft),
                          child: Text(
                            "Sign In",
                            textAlign: TextAlign.center,
                            style: TextStyle(fontSize: 30, color: Colors.white),
                          ),
                        ),
                        Container(
                          alignment: Alignment.center,

                          //margin: EdgeInsets.only(top: 10),
                          padding: EdgeInsets.only(
                              right: valPaddingRight, left: valPaddingLeft),
                          child: Text(
                            "let's choose your match",
                            textAlign: TextAlign.center,
                            style: TextStyle(fontSize: 15, color: Colors.amber),
                          ),
                        ),
                        SizedBox(
                          height: MediaQuery.of(context).size.height * 0.02,
                        ),
                        Container(
                          padding: EdgeInsets.only(
                              right: valPaddingRight, left: valPaddingLeft),
//                margin: EdgeInsets.only(top: 30),
                          margin: EdgeInsets.only(
                              top: MediaQuery.of(context).size.height * 0.01),
                          height: MediaQuery.of(context).size.height * 0.08,
                          child: TextFormField(
                            controller: emailEditingController,
                            /* validator: (value) {
                      if (value.isEmpty) {
                        return 'Please enter some text';
                      }
                      return null;
                    },*/
                            decoration: InputDecoration(
                              border: OutlineInputBorder(
                                  borderRadius:
                                      BorderRadius.all(Radius.circular(50.0))),
                              labelText: 'User Name',
                              hintStyle: TextStyle(color: Colors.deepOrange),
                              fillColor: Colors.black12,
                              filled: true,
                            ),
                          ),
                        ),
                        Container(
                          padding: EdgeInsets.only(
                              right: valPaddingRight, left: valPaddingLeft),
//                margin: EdgeInsets.only(top: 15),
                          margin: EdgeInsets.only(
                              top: MediaQuery.of(context).size.height * 0.01),
                          height: MediaQuery.of(context).size.height * 0.08,
                          child: TextField(
                            controller: psdEditingController,
                            //      maxLength: 6,
                            obscureText: true,
                            decoration: InputDecoration(
                              border: OutlineInputBorder(
                                  borderRadius:
                                      BorderRadius.all(Radius.circular(50.0))),
                              labelText: 'Password',
                              hintStyle: TextStyle(color: Colors.deepOrange),
                              fillColor: Colors.black12,
                              filled: true,
                            ),
                          ),
                        ),
                        Container(
                          alignment: Alignment.centerRight,
                          margin: EdgeInsets.only(
                              top: MediaQuery.of(context).size.height * 0.01),
                          padding: EdgeInsets.only(
                              right: valPaddingRight, left: valPaddingLeft),
                          child: Text(
                            "Forget Password?",
                            textAlign: TextAlign.right,
                            style: TextStyle(color: Colors.amber, fontSize: 15),
                          ),
                        ),
                        Container(
                            padding: EdgeInsets.only(
                                right: valPaddingRight, left: valPaddingLeft),
                            margin: EdgeInsets.only(
                                top: MediaQuery.of(context).size.height * 0.02),
                            child: SizedBox(
                              width: double.infinity,
                              height: MediaQuery.of(context).size.height * 0.08,
                              child: RaisedButton(
                                color: Colors.orange[400],
                                child: Text(
                                  'LOGIN',
                                  style: TextStyle(
                                      color: Colors.white, fontSize: 20),
                                ),
                                onPressed: () {
                                  setState(() {
                                    //   initState();
                                          chkFieldsValues(context);
                                  /*  if (isLoadingApi) {
                                      isLoadingApi = true;*/
                                     // showLoaderDialog(context);
                                   /* } else {
                                      isLoadingApi = false;
                                      Navigator.pop(context);
                                    }*/
                                  });
                                  //
                                },
                                shape: RoundedRectangleBorder(
                                  borderRadius: BorderRadius.circular(50.0),
                                  //   side: BorderSide(color: Colors.red)
                                ),
                              ),
                            )),
                        Container(
                          padding: EdgeInsets.only(
                              right: valPaddingRight, left: valPaddingLeft),
                          margin: EdgeInsets.only(
                              top: MediaQuery.of(context).size.height * 0.02),
                          child: Center(
                            child: Row(
                              crossAxisAlignment: CrossAxisAlignment.center,
                              mainAxisAlignment: MainAxisAlignment.center,
                              children: <Widget>[
                                Text(
                                  "Don't have an account?",
                                  textAlign: TextAlign.center,
                                ),
                                InkWell(
                                  child: Text(
                                    ' Sign Up Now',
                                    textAlign: TextAlign.center,
                                    style: TextStyle(color: Colors.amber),
                                  ),
                                  onTap: () {
                                    Navigator.of(context).push(
                                        MaterialPageRoute(
                                            builder: (BuildContext context) =>
                                                SignupScreen()));
                                  },
                                )
                              ],
                            ),
                          ),
                        ),
                        Container(
                          alignment: Alignment.center,
                          margin: EdgeInsets.only(
                              top: MediaQuery.of(context).size.height * 0.02),
                          height: MediaQuery.of(context).size.height * 0.02,
                          padding: EdgeInsets.only(
                              right: valPaddingRight, left: valPaddingLeft),
                          child: Text(
                            "-OR-",
                            textAlign: TextAlign.center,
                            style: TextStyle(fontSize: 12, color: Colors.white),
                          ),
                        ),
                        Container(
                          padding: EdgeInsets.only(
                              right: valPaddingRight, left: valPaddingLeft),
                          margin: EdgeInsets.only(
                              top: MediaQuery.of(context).size.height * 0.02),
                          child: SizedBox(
                            width: double.infinity,
                            height: MediaQuery.of(context).size.height * 0.06,
                            child: GestureDetector(
                              child: Container(
                                decoration: BoxDecoration(
                                  // color: Colors.black,
                                  image: DecorationImage(
                                      image:
                                          AssetImage("assets/images/fb_btn.png"),
                                      fit: BoxFit.cover),
                                ),
                              ),
                              onTap: () {
                                setState(() {
                                  if (_isFBLoggedIn) {
                                    _logout();
                                  } else {
                                    if (GlobalDataUtils()
                                            .isNetworkConnected() !=
                                        null) {
                                      _loginWithFB();
                                    } else {
                                      _showSnackBar(context,
                                          "Please check internet connection");
                                    }
                                  }
                                });
                              },
                              /*(
                            color: Colors.blueAccent,
                            child: Text(
                              'Login with facebook',
                              style: TextStyle(
                                  color: Colors.white, fontSize: 20),
                            ),
                            onPressed: () {
                              setState(() {
                                if (_isFBLoggedIn) {
                                  _logout();
                                }
                                else {
                                  if (GlobalDataUtils().isNetworkConnected() != null) {
                                    _loginWithFB();
                                  }
                                  else {
                                    _showSnackBar(context, "Please check internet connection");
                                  }
                                }
                              });
                            },
                            shape: RoundedRectangleBorder(
                              borderRadius: BorderRadius.circular(50.0),
                              //   side: BorderSide(color: Colors.red)
                            ),
                          ),*/
                            ),
                          ),
                        ),
                      ],
                    ),
                  ),
                ))));
  }

  Widget loadingView(bool isVisible) => Center(
        child: CircularProgressIndicator(
          backgroundColor: Colors.red,
        ),
      );

  void chkFieldsValues(BuildContext context) async {
    var xyz = WebApiCall().launchWebView();

    if (emailEditingController.text.isEmpty) {
      debugPrint("email is empty");
      _showSnackBar(context, "Please enter email ");
    } else if (psdEditingController.text.isEmpty) {
      debugPrint("Please enter password ");
      _showSnackBar(context, "Please enter password");
    } else {
      isLoadingApi = true;

      //loadingView(true);

      if (GlobalDataUtils().isNetworkConnected() != null) {
        Map mapData = {
          "email": emailEditingController.text.toString(),
          "password": psdEditingController.text.toString(),
          "facebook_id": "",
          "device_token": "xyzsdf",
          "device_type": "1",
          "first_name": ""
        };
        handleLoginApiResp(mapData);
      } else {
        _showSnackBar(context, "Please check internet connection");
      }

      if (isLoadingApi) {
        isLoadingApi = false;
      }
    }
  }

  _showSnackBar(BuildContext context, var a) {
    final snackBar = SnackBar(content: Text(a));
    _scaffoldKey.currentState.showSnackBar(snackBar);
  }

  showLoaderDialog(BuildContext context, bool chkForShow) {
if(chkForShow) {
  AlertDialog alert = AlertDialog(
    content: new Row(
      children: [
        CircularProgressIndicator(),
        Container(
            margin: EdgeInsets.only(left: 7), child: Text("Loading...")),
      ],
    ),
  );

  showDialog(
    barrierDismissible: false,
    context: context,
    builder: (BuildContext context) {
      return alert;
    },
  );
}
else {
  Navigator.of(context, rootNavigator: true).pop();
}
  }

  _loginWithFB() async {
    final result = await facebookLogin.logInWithReadPermissions(['email']);

    switch (result.status) {
      case FacebookLoginStatus.loggedIn:
        final token = result.accessToken.token;
        final graphResponse = await http.get(
            'https://graph.facebook.com/v2.12/me?fields=name,picture,email&access_token=${token}');
        final profile = JSON.jsonDecode(graphResponse.body);
        /*   bool isError = jsonDecoded['error'];
      String message = jsonDecoded['message'];*/
        String strName = profile['name'];
        String strEmail = profile['email'];
        String strId = profile['id'];

        print(profile);

        setState(() {
          userFaceBookProfile = profile;
          _isFBLoggedIn = true;
          Map mapData = {
            "email": "" + strEmail,
            "password": "",
            "facebook_id": "" + strId,
            "device_token": "xyzsdf",
            "device_type": "1",
            "first_name": "" + strName
          };
          handleLoginApiResp(mapData);
        });
        break;

      case FacebookLoginStatus.cancelledByUser:
        _showSnackBar(context, "Cancelled By User");
        setState(() => _isFBLoggedIn = false);
        break;
      case FacebookLoginStatus.error:
        _showSnackBar(context, "Something went wrong from Facebook");
        setState(() => _isFBLoggedIn = false);
        break;
    }
  }

  _logout() {
    facebookLogin.logOut();
    setState(() {
      _isFBLoggedIn = false;
    });
  }

  void handleLoginApiResp(Map mapData) async {
    showLoaderDialog(context,true);
    strLoginApiResponse = await WebApiCall().callPostApi("api/login",mapData,context);
    var jsonDecoded = json.decode(strLoginApiResponse);
    bool isError = jsonDecoded['error'];
    String message = jsonDecoded['message'];
    int status_code = jsonDecoded['status_code'];

    debugPrint("isError=${isError}");
    showLoaderDialog(context,false);

    if (isError == false && status_code==200) {
      loginApiResponse = loginPojoFromJson(strLoginApiResponse);
      debugPrint(loginApiResponse.data.sessionId.toString());
        SharedPreferences prefs = await SharedPreferences.getInstance();
      prefs.setString('sessionId', loginApiResponse.data.sessionId) ;

      StorageUtil.putString("test", "value");
      Navigator.of(context).pushReplacement(
          MaterialPageRoute(builder: (BuildContext context) => HomeScreen()));
    } else {
      debugPrint("message=${message}");
      _showSnackBar(context, "message ${message}");
    }
  }
}
