//import 'dart:html';

import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:mosh_match/LoginSignUpModule/OtpVerificationScreen.dart';
import 'package:mosh_match/ModelClasses/CountryListJson.dart';

class SignupScreen extends StatefulWidget {
  @override
  State<StatefulWidget> createState() {
    return _SignupScreen();
  }
}

class _SignupScreen extends State<SignupScreen> {
  var _cities = ['Mohali', 'Chandigarh', 'Panchkula', 'Patiala'];
  var choosedCity = "Chandigarh";
  var _country = ['India', 'Russia', 'US', 'Australia'];
  var choosedCountry = "Russia";
  var _gender = ['Male', 'Female'];
  var choosedGender = "Male";
  var countryListJson = List<CountryListJson>();

  var emailEditingController = TextEditingController();
  var psdEditingController = TextEditingController();
  var mobileEditingController = TextEditingController();
  var firstNameEditingController = TextEditingController();

  bool _isCountryVisible = true;
  bool _isCityVisible = false;
  bool _isDobVisible = false;
  bool _isEmailVisible = false;
  bool _isGenderVisible = false;
  bool _isMobileVisible = false;
  bool _isFirstNameVisible = false;
  bool _isPsdVisible = false;
  var btnClickedCount = 0;

  @override
  Widget build(BuildContext context) {
    var valPaddingLeft = MediaQuery.of(context).size.width * 0.05;
    var valPaddingRight = MediaQuery.of(context).size.width * 0.05;


   callCountryListApi();

    return MaterialApp(
        debugShowCheckedModeBanner: false,
        title: "MoshMatch",
        theme: ThemeData(
          // Define the default brightness and colors.
          brightness: Brightness.dark,
          primaryColor: Colors.orange,
          accentColor: Colors.amber,
          // Define the default font family.
          fontFamily: 'Georgia',
           textTheme: TextTheme(
            headline1: TextStyle(fontSize: 72.0, fontWeight: FontWeight.bold),
            headline6: TextStyle(fontSize: 36.0, fontStyle: FontStyle.italic),
            bodyText2: TextStyle(fontSize: 14.0, fontFamily: 'Hind'),
          ),
        ),
        home: Scaffold(
            body: Container(
          constraints: BoxConstraints.expand(),
          decoration: BoxDecoration(
              image: DecorationImage(
                  image: AssetImage("assets/images/bg.jpg"),
                  fit: BoxFit.cover)),
          child: ListView(
            children: [
              SizedBox(
                height: MediaQuery.of(context).size.height * 0.05,
              ),
              Image(
                image: AssetImage('assets/images/logo.png'),
                height: MediaQuery.of(context).size.height * 0.2,
              ),
              Container(
                alignment: Alignment.center,
                margin: EdgeInsets.only(
                    top: MediaQuery.of(context).size.height * 0.05),
                padding: EdgeInsets.only(
                    right: valPaddingRight, left: valPaddingLeft),
                child: Text(
                  "Sign Up",
                  textAlign: TextAlign.center,
                  style: TextStyle(
                    fontSize: 30,
                    color: Colors.white,
                    fontFamily: 'Schyler',
                    fontWeight: FontWeight.w900,
                  ),
                ),
              ),
              Container(
                alignment: Alignment.center,
                margin: EdgeInsets.only(
                    top: MediaQuery.of(context).size.height * 0.01),
                padding: EdgeInsets.only(
                    right: valPaddingRight, left: valPaddingLeft),
                child: Text(
                  "let's choose your match",
                  textAlign: TextAlign.center,
                  style: TextStyle(fontSize: 15, color: Colors.amber),
                ),
              ),
              SizedBox(
                height: MediaQuery.of(context).size.height * 0.15,
              ),
              Container(
                padding: EdgeInsets.only(
                    right: valPaddingRight, left: valPaddingLeft),
                margin: EdgeInsets.only(top: 15),
                child: Container(
                  //  padding: EdgeInsets.symmetric(horizontal: 10.0),
                  height: MediaQuery.of(context).size.height * 0.1,
                  decoration: BoxDecoration(
                      borderRadius: BorderRadius.circular(50.0),
                      border: Border.all(
                          color: Colors.indigo,
                          style: BorderStyle.solid,
                          width: 0.80),
                      color: Color(0xff0A1E37)
                      /*#0A1E37*/
                      ),
                  child: Stack(
                    children: [
                      Visibility(
                        visible: _isCountryVisible,
                        child: Center(
                          child: DropdownButton(
                            underline: SizedBox(),
                            items: _country
                                .map((value) => DropdownMenuItem(
                                      child: Text("${value}"),
                                      value: value,
                                    ))
                                .toList(),
                            onChanged: (String value) {
                              choosedCountry = value;
                            },
                            isExpanded: false,
                            value: choosedCountry,
                          ),
                        ),
                      ),
                      Visibility(
                        visible: _isCityVisible,
                        child: Center(
                          child: DropdownButton(
                            underline: SizedBox(),
                            items: _cities
                                .map((value) => DropdownMenuItem(
                                      child: Text(value),
                                      value: value,
                                    ))
                                .toList(),
                            onChanged: (String value) {
                              choosedCity = value;
                            },
                            isExpanded: false,
                            value: choosedCity,
                          ),
                        ),
                      ),
                      Visibility(
                        visible: _isDobVisible,
                        child: Center(
                          child: InkWell(
                            child: Text(
                              "dd/mm/yyyy",
                            ),
                            onTap: () {
                              debugPrint("date of birth choosed");
                            },
                          ),
                        ),
                      ),
                      Visibility(
                        visible: _isEmailVisible,
                        child: Center(
                          child: TextField(
                            textAlign: TextAlign.center,
                            controller: emailEditingController,
                            autofocus: true,
                            decoration: InputDecoration(
                                labelText: 'Email',
                                hintStyle: TextStyle(
                                    color: Colors.deepOrange,
                                    fontFamily: 'Schyler'),
                                fillColor: Colors.black12,
                                filled: true,
                                border: InputBorder.none),
                          ),
                        ),
                      ),
                      Visibility(
                        visible: _isGenderVisible,
                        child: Center(
                          child: DropdownButton(
                            underline: SizedBox(),
                            items: _gender
                                .map((value) => DropdownMenuItem(
                                      child: Text(value),
                                      value: value,
                                    ))
                                .toList(),
                            onChanged: (String value) {
                              choosedGender = value;
                            },
                            isExpanded: false,
                            value: choosedGender,
                          ),
                        ),
                      ),
                      Visibility(
                        visible: _isMobileVisible,
                        child: Center(
                          child: TextField(
                            controller: mobileEditingController,
                            //      maxLength: 6,
                            obscureText: true,
                            textAlign: TextAlign.center,
                            autofocus: true,
                            decoration: InputDecoration(
                                labelText: 'Mobile',
                                hintStyle: TextStyle(color: Colors.deepOrange),
                                fillColor: Colors.black12,
                                filled: true,
                                border: InputBorder.none),
                          ),
                        ),
                      ),
                      Visibility(
                        visible: _isFirstNameVisible,
                        child: Center(
                          child: TextField(
                            controller: mobileEditingController,
                            textAlign: TextAlign.center,
                            //      maxLength: 6,
                            //   obscureText: true,
                            autofocus: true,
                            decoration: InputDecoration(
                                labelText: 'First Name',
                                hintStyle: TextStyle(color: Colors.deepOrange),
                                fillColor: Colors.black12,
                                filled: true,
                                border: InputBorder.none),
                          ),
                        ),
                      ),
                      Visibility(
                        visible: _isPsdVisible,
                        child: Center(
                          child: TextField(
                            autofocus: true,
                            textAlign: TextAlign.center,
                            controller: psdEditingController,
                            obscureText: true,
                            decoration: InputDecoration(
                                labelText: 'Password',
                                hintStyle: TextStyle(color: Colors.deepOrange),
                                fillColor: Colors.black12,
                                filled: true,
                                border: InputBorder.none),
                          ),
                        ),
                      ),
                    ],
                  ),
                ),
              ),
              Container(
                  padding: EdgeInsets.only(
                      right: valPaddingRight, left: valPaddingLeft),
                  margin: EdgeInsets.only(
                      top: MediaQuery.of(context).size.height * 0.03),
                  child: SizedBox(
                    width: double.infinity,
                    height: MediaQuery.of(context).size.height * 0.08,
                    child: RaisedButton(
                      color: Colors.orange[400],
                      child: Text(
                        'CONTINUE',
                        style: TextStyle(color: Colors.white, fontSize: 20),
                      ),
                      onPressed: () {
                        showSnackBar(context);
                        btnContinuePressed();
                      },
                      shape: RoundedRectangleBorder(
                        borderRadius: BorderRadius.circular(50.0),
                        //   side: BorderSide(color: Colors.red)
                      ),
                    ),
                  )),
              Container(
                padding: EdgeInsets.only(
                    right: valPaddingRight, left: valPaddingLeft),
                margin: EdgeInsets.only(
                    top: MediaQuery.of(context).size.height * 0.03),
                child: Center(
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.center,
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: [
                      Text(
                        "By Tapping Log In, you agree with our",
                        textAlign: TextAlign.center,
                      ),
                      Row(
                        crossAxisAlignment: CrossAxisAlignment.center,
                        mainAxisAlignment: MainAxisAlignment.center,
                        children: [
                          InkWell(
                            child: Text(
                              "Term of Service",
                              textAlign: TextAlign.center,
                              style: TextStyle(color: Colors.amber),
                            ),
                            onTap: () {
                              //  showSnackBar(context);
                              debugPrint("Term of service clicked");
                            },
                          ),
                          Text(
                            ' and ',
                            textAlign: TextAlign.center,
                            style: TextStyle(color: Colors.white),
                          ),
                          Text(
                            "Privacy Policy.",
                            textAlign: TextAlign.center,
                            style: TextStyle(color: Colors.amber),
                          ),
                        ],
                      ),
                      /* Text(
                            ' Sign Up Now',
                            textAlign: TextAlign.center, style: TextStyle(color: Colors.amber),
                          ),*/
                    ],
                  ),
                ),
              ),
            ],
          ),
        )));
  }

  void btnContinuePressed() {
    if (btnClickedCount == 0) {
      hideAllVisibility();
      _isCountryVisible = true;
    } else if (btnClickedCount == 1) {
      hideAllVisibility();
      _isCityVisible = true;
    } else if (btnClickedCount == 2) {
      hideAllVisibility();
      _isDobVisible = true;
    } else if (btnClickedCount == 3) {
      hideAllVisibility();
      _isEmailVisible = true;
    } else if (btnClickedCount == 4) {
      hideAllVisibility();
      _isGenderVisible = true;
    } else if (btnClickedCount == 5) {
      hideAllVisibility();
      _isMobileVisible = true;
    } else if (btnClickedCount == 6) {
      hideAllVisibility();
      _isFirstNameVisible = true;
    } else if (btnClickedCount == 7) {
      hideAllVisibility();
      _isPsdVisible = true;
    }
    btnClickedCount++;

    if (btnClickedCount > 8) {
      Navigator.of(context).push(MaterialPageRoute(
          builder: (BuildContext context) => OtpVerificationScreen()));
    }
  }

  void hideAllVisibility() {
    setState(() {
      _isCountryVisible = false;
      _isCityVisible = false;
      _isDobVisible = false;
      _isEmailVisible = false;
      _isGenderVisible = false;
      _isMobileVisible = false;
      _isFirstNameVisible = false;
      _isPsdVisible = false;
    });
  }

 }

void callCountryListApi() {
 // country

}


void showSnackBar(BuildContext context) {
  var snackBar = SnackBar(
    content: Text("You just tapped Button"),
    action: SnackBarAction(
      label: "Undo",
      onPressed: () {
        debugPrint("Snack Bar Undo pressed ");
      },
    ),
  );
  Scaffold.of(context).showSnackBar(snackBar);
  /*final snackBar = SnackBar(content: Text('Are you talkin\' to me?'));
     Scaffold.of(context).showSnackBar(snackBar); */

  void _showSnackBar(BuildContext context, String message) {
    final snackBar = SnackBar(content: Text(message));
    Scaffold.of(context).showSnackBar(snackBar);
  }
}
