import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'file:///D:/Android%20Projects/Office%20Projects/New%20Projects/Flutter/MoshMatch%20all%20things/mosh_match/lib/HomeData/HomeScreen.dart';

class CongractsRequestSent extends StatefulWidget {
  @override
  State<StatefulWidget> createState() {
    return _CongractsRequestSentState();
  }
}

class _CongractsRequestSentState extends State<CongractsRequestSent> {
  @override
  Widget build(BuildContext context) {
    var amberClr=Colors.amber;
    var whiteClr=Colors.white;


    var valPaddingLeft=MediaQuery.of(context).size.width * 0.05;
    var valPaddingRight=MediaQuery.of(context).size.width * 0.05;
    return MaterialApp(
        title: "MoshMatch",
        debugShowCheckedModeBanner: false,
        theme: ThemeData(
          // Define the default brightness and colors.
          brightness: Brightness.dark,
          primaryColor: Colors.orange,
          accentColor: Colors.amber,
          // Define the default font family.
          fontFamily: 'Georgia',
          // Define the default TextTheme. Use this to specify the default
          // text styling for headlines, titles, bodies of text, and more.
          textTheme: TextTheme(
            headline1: TextStyle(fontSize: 72.0, fontWeight: FontWeight.bold),
            headline6: TextStyle(fontSize: 36.0, fontStyle: FontStyle.italic),
            bodyText2: TextStyle(fontSize: 14.0, fontFamily: 'Hind'),
          ),
        ),
        home: Scaffold(
            body: Container(
              constraints: BoxConstraints.expand(),
              decoration: BoxDecoration(
                  image: DecorationImage(
                      image: AssetImage("assets/images/bg.jpg"),
                      fit: BoxFit.cover)
              ),
              child: Column(
                children: [
                  SizedBox(
                    height: MediaQuery.of(context).size.height * 0.2,
                  ),
                  Image(
                    image: AssetImage('assets/images/logo.png'),
                    height: MediaQuery.of(context).size.height * 0.3,
                  ),
                  SizedBox(
                    height: MediaQuery.of(context).size.height * 0.1,
                  ),
                  Container(
                    alignment: Alignment.center,
                    padding: EdgeInsets.only(right: valPaddingRight, left: valPaddingLeft),
                    child: Text(
                      "Congractulations",
                      textAlign: TextAlign.center,
                      style: TextStyle(fontSize: 30, color: amberClr),
                    ),
                  ),
                  Container(
                    alignment: Alignment.center,
                    margin: EdgeInsets.only(top: MediaQuery.of(context).size.height * 0.02),
                    padding: EdgeInsets.only(right: valPaddingRight, left: valPaddingLeft),
                    child: Text(
                      "Mutual sympathy. Do not waste\n time and write to her",
                      textAlign: TextAlign.center,
                      style: TextStyle(fontSize: 15, color: whiteClr),
                    ),
                  ),
                   SizedBox(
                    height: MediaQuery.of(context).size.height * 0.05,
                  ),
                  Container(
                      padding: EdgeInsets.only(right: 20, left: 20),
                      child: SizedBox(
                        width: double.infinity,
                        height: MediaQuery.of(context).size.height * 0.06,
                        child: RaisedButton(
                          color: Colors.orange[400],
                          child: Text(
                            'WRITE TO MARLENE',
                            style: TextStyle(color: Colors.white, fontSize: 20),
                          ),
                          onPressed: () {

                          },
                          shape: RoundedRectangleBorder(
                            borderRadius: BorderRadius.circular(50.0),
                            //   side: BorderSide(color: Colors.red)
                          ),
                        ),
                      )),
                  SizedBox(
                    height: MediaQuery.of(context).size.height * 0.05,
                  ),   Container(
                    alignment: Alignment.center,
                     padding: EdgeInsets.only(right: valPaddingRight, left: valPaddingLeft),
                    child: InkWell (
                      child: Text(
                        "Back to Search",
                        textAlign: TextAlign.center,
                        style: TextStyle(fontSize: 15, color: amberClr),
                      ),
                      onTap: (){
                        Navigator.of(context).pushReplacement(MaterialPageRoute(
                            builder: (BuildContext context) =>
                                HomeScreen()));
                      },
                    )
                  ),
                ],
              ),
            )
        )
    );
  }
}
