import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:mosh_match/LoginSignUpModule/ConfirmPSdChange.dart';
import 'package:otp_text_field/otp_field.dart';
import 'package:otp_text_field/style.dart';

class OtpVerificationScreen extends StatefulWidget {
  @override
  State<StatefulWidget> createState() {
    return _OtpVerificationScreenState();
  }
}

class _OtpVerificationScreenState extends State<OtpVerificationScreen>  {
  var emailEditingController = TextEditingController();
  var counterDownVal = 60;
var amberColor =Colors.amber;
  @override
  Widget build(BuildContext context) {

    var valPaddingLeft=MediaQuery.of(context).size.width * 0.05;
    var valPaddingRight=MediaQuery.of(context).size.width * 0.05;

    return MaterialApp(
        title: "MoshMatch",
debugShowCheckedModeBanner: false,
        theme: ThemeData(
          // Define the default brightness and colors.
          brightness: Brightness.dark,
          primaryColor: Colors.orange,
          accentColor: amberColor,
          // Define the default font family.
          fontFamily: 'Georgia',
          // Define the default TextTheme. Use this to specify the default
          // text styling for headlines, titles, bodies of text, and more.
          textTheme: TextTheme(
            headline1: TextStyle(fontSize: 72.0, fontWeight: FontWeight.bold),
            headline6: TextStyle(fontSize: 36.0, fontStyle: FontStyle.italic),
            bodyText2: TextStyle(fontSize: 14.0, fontFamily: 'Hind'),
          ),
        ),
        home: Scaffold(

    appBar: AppBar(
            backgroundColor: Color(0xff1C1C1C),
            title: Text("Profile Verification" ,  style:  TextStyle(color: Colors.white),  ),
leading:  IconButton(icon: Icon(Icons.arrow_back_ios),
  color: Colors.white,
  onPressed: () {
    debugPrint("on tapped profile verification");
    },
),
          ),
            body: Container(
          constraints: BoxConstraints.expand(),
          decoration: BoxDecoration(
              image: DecorationImage(
                  image: AssetImage("assets/images/bg.jpg"),
                  fit: BoxFit.cover)
          ),
          child: ListView(
            children: [
              SizedBox(
                height: MediaQuery.of(context).size.width * 0.1,
              ),
              Container(
                alignment: Alignment.centerLeft,
                padding: EdgeInsets.only(right: valPaddingRight, left: valPaddingLeft),
                child: Text(
                  "Verify OTP",
                  textAlign: TextAlign.left,
                  style: TextStyle(fontSize: 18, color: Colors.white),
                ),
              ),
              SizedBox(
                height: MediaQuery.of(context).size.width * 0.1,
              ),
              Container(
                alignment: Alignment.centerLeft,
                padding: EdgeInsets.only(right: valPaddingRight, left: valPaddingLeft),
                child: Text(
                  "Enter Your OTP here...",
                  textAlign: TextAlign.left,
                  style: TextStyle(fontSize: 18, color: Colors.white),
                ),
              ),
              Container(
                alignment: Alignment.centerLeft,
                height: MediaQuery.of(context).size.width * 0.05,

                padding: EdgeInsets.only(right: valPaddingRight, left: valPaddingLeft),
                child: Text(
                  "Your OTP will be valid for ${counterDownVal} seconds",
                  textAlign: TextAlign.left,
                  style: TextStyle(fontSize: 13, color: Colors.white),
                ),
              ),
              /*SizedBox(
                    height: 30,
                  ),*/
              Container(
                alignment: Alignment.centerLeft,
                margin: EdgeInsets.only(top: MediaQuery.of(context).size.width * 0.01),
                padding: EdgeInsets.only(right: valPaddingRight, left: valPaddingLeft),
                child: OTPTextField(
                  length: 4,
                  width: MediaQuery.of(context).size.width * 4 / 6,
                  textFieldAlignment: MainAxisAlignment.spaceAround,
                  fieldWidth: MediaQuery.of(context).size.width / 7,
                  fieldStyle: FieldStyle.box,
                  style: TextStyle(fontSize: 14),
                  onCompleted: (pin) {
                    print("Completed: " + pin);
                  },
                ),
              ),
              SizedBox(
                height: MediaQuery.of(context).size.width * 0.08,
              ),
              Container(
                  padding: EdgeInsets.only(right: valPaddingRight, left: valPaddingLeft),
                   child: SizedBox(
                    width: double.infinity,
                    height: MediaQuery.of(context).size.width * 0.15,
                    child: RaisedButton(
                      color: Colors.orange[400],
                      child: Text(
                        'UPDATE',
                        style: TextStyle(color: Colors.white, fontSize: 20),
                      ),
                      onPressed: () {
                        Navigator.of(context).pushReplacement(MaterialPageRoute(
                            builder: (BuildContext context) => ConfirmPSDChange()
                        )
                        );
                      },
                      shape: RoundedRectangleBorder(
                        borderRadius: BorderRadius.circular(50.0),
                        //   side: BorderSide(color: Colors.red)
                      ),
                    ),
                  )),
              Container(
                padding: EdgeInsets.only(right: valPaddingRight, left: valPaddingLeft),
                margin: EdgeInsets.only(top: MediaQuery.of(context).size.width * 0.08),
                child: Center(
                  child: Row(
                    crossAxisAlignment: CrossAxisAlignment.center,
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: [
                      InkWell(
                        child: Text(
                          '  Resend OTP  ',
                          textAlign: TextAlign.center,
                          style: TextStyle(
                            color: amberColor,
                            decoration: TextDecoration.underline,
                          ),
                        ),
                        onTap: () {
                          /* Navigator.of(context).push(MaterialPageRoute(
                                  builder: (BuildContext context) =>
                                      SignupScreen()));*/
                        },
                      )
                    ],
                  ),
                ),
              ),
            ],
          ),
        )));
  }
}
