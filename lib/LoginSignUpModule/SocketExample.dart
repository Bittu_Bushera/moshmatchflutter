import 'package:adhara_socket_io/adhara_socket_io.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

class SocketExample extends StatefulWidget {
  @override
  State<StatefulWidget> createState() {
    return _SocketExampleState();
  }
}

class _SocketExampleState extends State<SocketExample> {
  final msgEditingController = TextEditingController();
  List<String> toPrint = ["trying to connect"];
  SocketIOManager manager = SocketIOManager();
  SocketIO socket;

  Map<String, SocketIO> sockets = {};

  @override
  void initState() {
    super.initState();
    manager = SocketIOManager();
    chkFieldsValues(context);
  }

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
        title: "MoshMatch",
        theme: ThemeData(
          // Define the default brightness and colors.
          brightness: Brightness.dark,
          primaryColor: Colors.orange,
          accentColor: Colors.amber,
          // Define the default font family.
          fontFamily: 'Georgia',
          // Define the default TextTheme. Use this to specify the default
          // text styling for headlines, titles, bodies of text, and more.
          textTheme: TextTheme(
            headline1: TextStyle(fontSize: 72.0, fontWeight: FontWeight.bold),
            headline6: TextStyle(fontSize: 36.0, fontStyle: FontStyle.italic),
            bodyText2: TextStyle(fontSize: 14.0, fontFamily: 'Hind'),
          ),
        ),
        home: Scaffold(
            body: Container(
          constraints: BoxConstraints.expand(),
          decoration: BoxDecoration(
              image: DecorationImage(
                  image: AssetImage("assets/images/bg.jpg"),
                  fit: BoxFit.cover)),
          child: ListView(
            children: [
              SizedBox(
                height: 30,
              ),
              Image(
                image: AssetImage('assets/images/logo.png'),
                height: 150,
                width: 150,
              ),
              Container(
                padding: EdgeInsets.only(right: 20, left: 20),
                margin: EdgeInsets.only(top: 30),
                child: TextField(
                  controller: msgEditingController,
                  decoration: InputDecoration(
                    border: OutlineInputBorder(
                        borderRadius: BorderRadius.all(Radius.circular(50.0))),
                    labelText: 'User Name',
                    hintStyle: TextStyle(color: Colors.deepOrange),
                    fillColor: Colors.black12,
                    filled: true,
                  ),
                ),
              ),
              Container(
                  padding: EdgeInsets.only(right: 20, left: 20),
                  margin: EdgeInsets.only(top: 20),
                  child: SizedBox(
                    width: double.infinity,
                    height: 50,
                    child: RaisedButton(
                      color: Colors.orange[400],
                      child: Text(
                        'Send Message',
                        style: TextStyle(color: Colors.white, fontSize: 20),
                      ),
                      onPressed: () {
                        chkFieldsValues(context);
                        //                           initState();
                      },
                      shape: RoundedRectangleBorder(
                        borderRadius: BorderRadius.circular(50.0),
                        //   side: BorderSide(color: Colors.red)
                      ),
                    ),
                  )),
            ],
          ),
        )));
  }

  void chkFieldsValues(BuildContext context) async {
    if (msgEditingController.text.toString().isEmpty) {
      /*  final snackBar = SnackBar(content: Text('field is empty ?'));
      Scaffold.of(context).showSnackBar(snackBar);*/
      debugPrint("field is empty");
    } else {
      socket = await manager
          .createInstance(SocketOptions('http://167.172.209.57:4000/'));   //http://167.172.209.57:3000/
      socket.onConnect((data) {
        pprint("connected...");
        pprint("onConnectData= " + data.toString());
        // "session_id" : "j257YOeLdTDLEZqQ6TzQAKmllnQFIMV1"  userid = 3
        //  session_id= ZSfSCVD8IeHR1eVcCTqx2NPOJR9PLDl8  userid = 5
  /*      socket.emit('authenticate', [
          {"session_id": 'j257YOeLdTDLEZqQ6TzQAKmllnQFIMV1', "reciever_id": '5'}
        ]);*/
        var responseData = [
          {
            "session_id": "y4Y3NPk8u4QjBYVD3sn1KIsM5nyVtQkc",
            "reciever_id": "5",
            "message": "${msgEditingController.text} from android team " +
                DateTime.now().toString(),"media":""
          }
        ];

        pprint("inputData=" + responseData.toString());
        socket.emit('send_message', responseData);

        socket.on('getMessage', (data) {
          // hera  is where you receive from `server`
          pprint('getMessage');
          pprint("getMessage listened=" + data.toString());
        });

        socket.on("message_data1", (data) {
          pprint('message_data1');

          pprint("message_data1 listened data= " +  data.toString());
        });

        socket.on("getMessageNew",
            (data){
          pprint("getMessageNew listened data= " + data);
            });
      });

      socket.connect();
      pprint("socket.isConnected()= ${socket.isConnected()} ");

      /*
 if(socket.isConnected()==false)
{

}
 else{
   pprint("socket.isConnected()= ${socket.isConnected()} ");

 }
*/
    }
  }
}

void pprint(String s) {
  debugPrint(s);
}



class Photo {
  final int albumId;
  final int id;
  final String title;
  final String url;
  final String thumbnailUrl;

  Photo({this.albumId, this.id, this.title, this.url, this.thumbnailUrl});

  factory Photo.fromJson(Map<String, dynamic> json) {
    return Photo(
      albumId: json['albumId'] as int,
      id: json['id'] as int,
      title: json['title'] as String,
      url: json['url'] as String,
      thumbnailUrl: json['thumbnailUrl'] as String,
    );
  }
}
