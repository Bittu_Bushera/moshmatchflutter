

import 'package:connectivity/connectivity.dart';
import 'package:flutter/material.dart';

class GlobalDataUtils  {
  var _scaffoldKey = GlobalKey<ScaffoldState>();


void snackBarShow (ctx,strMsg,GlobalKey glblKey )
{
  final snackBar = SnackBar(content: Text(strMsg));
  _scaffoldKey.currentState.showSnackBar(snackBar);
}


  Future<bool> isNetworkConnected() async {
    var connectivityResult = await (Connectivity().checkConnectivity());
    if (connectivityResult == ConnectivityResult.mobile) {
      return true;
    } else if (connectivityResult == ConnectivityResult.wifi) {
      return true;
    }
    return false;
  }

}