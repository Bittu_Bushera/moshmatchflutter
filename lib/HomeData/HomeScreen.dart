import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:mosh_match/HomeData/FriendsSuggestion.dart';
import 'package:mosh_match/HomeData/UserLstForChat.dart';
import 'package:mosh_match/ModelClasses/UserListForChatResp.dart';
import 'package:mosh_match/StorageUtil.dart';
import 'package:shared_preferences/shared_preferences.dart';

class HomeScreen extends StatefulWidget {
  @override
  State<StatefulWidget> createState() {
    return _HomeScreenState();
  }
}

class _HomeScreenState extends State<HomeScreen> {
  int _currentIndex = 2;

  final List<Widget> _children = [
    FriendsSuggestion(Colors.amber),
    FriendsSuggestion(Colors.green),
    UserLstForChat(),
    FriendsSuggestion(Colors.pink),
  ];

  @override
  void initState() {
    chkSavedVal();
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        /* appBar: AppBar(
          title: Text('My Flutter App'),
        ),*/
        body: _children[_currentIndex], // new
        bottomNavigationBar: new Theme(
            data: Theme.of(context).copyWith(
                // sets the background color of the `BottomNavigationBar`
                canvasColor: Color(0xFF102A45),
                // sets the active color of the `BottomNavigationBar` if `Brightness` is light
                primaryColor: Colors.amber,
                textTheme: Theme.of(context)
                    .textTheme
                    .copyWith(caption: new TextStyle(color: Colors.white))),
            // sets the
            child: new BottomNavigationBar(
              onTap: onTabTapped, // new
              currentIndex: _currentIndex, // new
              type: BottomNavigationBarType.fixed,
              items: [
                new BottomNavigationBarItem(
                  icon: ImageIcon(
                      AssetImage("assets/images/explore_icon.png")),
                  title: Text('Explore'),
                ),
                new BottomNavigationBarItem(
                  icon: Icon(Icons.favorite),
                  title: Text('Matches'),
                ),
                new BottomNavigationBarItem(
                    icon: ImageIcon(
                        AssetImage("assets/images/chat_icon.png")), title: Text('chat')),
                new BottomNavigationBarItem(
                    icon: Icon(Icons.person_rounded), title: Text('Profile'))
              ],
            )
            /*explore   matches chat profile*/
            ));
  }

  void onTabTapped(int index) {
    setState(() {
      _currentIndex = index;
    });
  }
}

void chkSavedVal() async {
  SharedPreferences prefs = await SharedPreferences.getInstance();
  print("Home screen  " + prefs.getString('sessionId'));
  print("Home screen test  " + StorageUtil.getString("test"));
}
