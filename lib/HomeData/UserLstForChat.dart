import 'dart:convert';

import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:mosh_match/ChatData/ChatDetail.dart';
import 'package:mosh_match/GlobalDataUtils.dart';
import 'package:mosh_match/HomeData/FriendsSuggestion.dart';
import 'package:mosh_match/HomeData/HomeScreen.dart';
import 'package:mosh_match/ModelClasses/UserListForChatResp.dart';
import 'package:mosh_match/webApiCall.dart';
import 'package:shared_preferences/shared_preferences.dart';
bool holdVar;

class UserLstForChat extends StatefulWidget {
  UserLstForChat( ){
    // holdVar=gotVar;

  }

  @override
  State<StatefulWidget> createState() {
    return _UserLstForChatState(holdVar);
  }
}

class _UserLstForChatState extends State<UserLstForChat> {
  String strSessionId = "";
  String TAG = "_UserLstForChatState ";

  var holdVar2;

  _UserLstForChatState(bool val) {
    holdVar2 = val;
  }

  var _scaffoldKey = GlobalKey<ScaffoldState>();
  String strUserForChatApiResponse = "";
  List<UserListForChatModel> _usersList = List<UserListForChatModel>();


  @override
  Widget build(BuildContext context) {
    holdVar2 = false;
    var valPaddingLeft = MediaQuery
        .of(context)
        .size
        .width * 0.05;
    var valPaddingRight = MediaQuery
        .of(context)
        .size
        .width * 0.05;

    debugPrint("user list chat screen");

    if (strUserForChatApiResponse == "") {
      getAllUsersApi();
    }

    return    SafeArea(
      child: Scaffold(
        backgroundColor: Colors.white,
          key: _scaffoldKey,
          body:
          Stack(
            children: <Widget>[
              Container(
                width: MediaQuery.of(context).size.width,
                height: MediaQuery.of(context).size.height,
                constraints: BoxConstraints.expand(),
                decoration: BoxDecoration(
                    image: DecorationImage(
                        image: AssetImage("assets/images/bg.jpg"),
                        fit: BoxFit.cover)),
              ),

              ListView.builder(
                itemCount: _usersList.length,
                itemBuilder: _buildItemsForListView,

              )
            ],
          )

      ),)
 /*   MaterialApp(
        theme: ThemeData(
          brightness: Brightness.dark,
          primaryColor: Colors.orange,
          accentColor: Colors.amber,
          // Define the default font family.
          fontFamily: 'Georgia',
          // Define the default TextTheme. Use this to specify the default
          // text styling for headlines, titles, bodies of text, and more.
          textTheme: TextTheme(
            headline1: TextStyle(fontSize: 72.0, fontWeight: FontWeight.bold),
            headline6: TextStyle(fontSize: 36.0, fontStyle: FontStyle.italic),
            bodyText2: TextStyle(fontSize: 14.0, fontFamily: 'Hind'),
          ),
        ),
        debugShowCheckedModeBanner: false,
        title: "Mosh Match",
        home:


     )*/;
  }


  Widget _buildItemsForListView(BuildContext context, int index) {
     return  GestureDetector(
         onTap: () {
           Navigator.of(context).push(MaterialPageRoute(
               builder: (context) => ChatDetail(_usersList[index])));

         },
      child:Container(

        margin: EdgeInsets.only(top: 10.0),
          height: 90,
          child: Row (
            children:<Widget> [
              Image(image: AssetImage("assets/images/logo.png",
              ),


              ),

              Center(

                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.center,
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: <Widget>[
                   Row(children: <Widget>[
                     Text(
                       _usersList[index].firstName + " " + _usersList[index].lastName,
                       textAlign: TextAlign.center,
                       style: TextStyle(fontSize: 12,color: Colors.white),
                     ),

              /* Container(
                 alignment: Alignment.centerRight,
                 child:       Text(
                 "2 hrs",
                  textAlign: TextAlign.right,
                 style: TextStyle(fontSize: 12,color: Colors.white),
               ),*/
                    //)
                   ]),


                    Text(
                        _usersList[index].email  ,
                        textAlign: TextAlign.center,
                        style: TextStyle(fontSize: 8,color: Colors.white,fontStyle:FontStyle.normal)
                    ),
                  ],
                ),
              )
            ],
          )
      )

     );
    /*ListTile(

      leading: Icon(Icons.star,color: Colors.white,),
      title: Text(
          _usersList[index].firstName + " " + _usersList[index].lastName,
          style: TextStyle(fontSize: 12,color: Colors.white)),
      subtitle: Text(_usersList[index].phone, style: TextStyle(fontSize: 12,color: Colors.white)),

      onTap: () {
        Navigator.of(context).push(MaterialPageRoute(
            builder: (context) => ChatDetail(_usersList[index])));

      },
    );*/

   }

   Widget viewList(){
  return SizedBox(
    height: 100,

  );
  }

  showLoaderDialog(BuildContext ctx, bool chkForShow) {
    if (chkForShow) {
      AlertDialog alert = AlertDialog(
        content: new Row(
          children: [
            CircularProgressIndicator(),
            Container(
                margin: EdgeInsets.only(left: 7), child: Text("Loading...")),
          ],
        ),
      );

      showDialog(
        barrierDismissible: false,
        context: ctx,
        builder: (BuildContext context) {
          return alert;
        },
      );
    }
    else {
      Navigator.of(ctx, rootNavigator: true).pop();
    }
  }

  void getAllUsersApi() async {
    //  showLoaderDialog(context,true);

    SharedPreferences prefs = await SharedPreferences.getInstance();
    strSessionId=""+prefs.getString('sessionId');
    print(   TAG+" sessionId="+strSessionId     );


    Map mapData = {
      "session_id":strSessionId
    };
    var apiResponse = await WebApiCall().callPostApi(
        "api/userList", mapData, context);
    setState(() {
      strUserForChatApiResponse = apiResponse;
      var jsonDecoded = json.decode(strUserForChatApiResponse);
      bool isError = jsonDecoded['error'];
      String message = jsonDecoded['message'];

      debugPrint("isError=${isError}  \n strData=${_usersList.length}");
      if (isError == false) {
        UserListForChatResp userListForChatResp = userListForChatRespFromJson(
            strUserForChatApiResponse);
        _usersList = userListForChatResp.data;
        debugPrint("getAllUsersApi " + _usersList.length.toString());

        /*
          loginApiResponse = loginPojoFromJson(strLoginApiResponse);*/
      }
    });

    // showLoaderDialog(context,false);
  }



}