import 'package:flutter/cupertino.dart';

class FriendsSuggestion extends StatelessWidget {
  final Color color;

  FriendsSuggestion(this.color);

  @override
  Widget build(BuildContext context) {
    return Container(
      color: color,
    );
  }
}