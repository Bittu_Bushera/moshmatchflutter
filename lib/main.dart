import 'dart:async';
import 'package:flutter/material.dart';
import 'package:mosh_match/LoginSignUpModule/SocketExample.dart';
import 'file:///D:/Android%20Projects/Office%20Projects/New%20Projects/Flutter/MoshMatch%20all%20things/mosh_match/lib/HomeData/HomeScreen.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'LoginSignUpModule/LoginScreen.dart';

void main() {
  // SharedPreferences.setMockInitialValues({});

  runApp(MaterialApp(
      theme: ThemeData(
        brightness: Brightness.dark,
        primaryColor: Colors.orange,
        accentColor: Colors.amber,
        // Define the default font family.
        fontFamily: 'Georgia',
        // Define the default TextTheme. Use this to specify the default
        // text styling for headlines, titles, bodies of text, and more.
        textTheme: TextTheme(
          headline1: TextStyle(fontSize: 72.0, fontWeight: FontWeight.bold),
          headline6: TextStyle(fontSize: 36.0, fontStyle: FontStyle.italic),
          bodyText2: TextStyle(fontSize: 14.0, fontFamily: 'Hind'),
        ),
      ),
      debugShowCheckedModeBanner: false,
      title: "Mosh Match",
      home: MyApp()));
}

class ImageLoad extends StatelessWidget {
  String TAG = "maindart  ";
  String strSessionId = "";

  @override
  Widget build(BuildContext context) {
    getsharedVal();
    Timer(Duration(seconds: 3), () {
      checkLoggedValue(context);
    });
    AssetImage assetImage = AssetImage('assets/images/splash.jpg');
    Image img = Image(
      image: assetImage,
    );
    return img;
  }

  void checkLoggedValue(BuildContext context) async {
  /*  Navigator.of(context).push(MaterialPageRoute(
        builder: (BuildContext context) => SocketExample()
    ));*/
    if (strSessionId.isEmpty) {
      debugPrint("if condition");

      Navigator.of(context).pushReplacement(
          MaterialPageRoute(builder: (BuildContext context) => LoginScreen()));
    } else {
      debugPrint("else condition");
      Navigator.of(context).push(
          MaterialPageRoute(builder: (BuildContext context) => HomeScreen()));
    }
  }

  void getsharedVal() async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    strSessionId = "" + prefs.getString('sessionId');
    debugPrint(TAG + " sessionId=" + strSessionId);
  }
}

class MyApp extends StatefulWidget {
  @override
  _MyAppState createState() => _MyAppState();
}

class _MyAppState extends State<MyApp> {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      debugShowCheckedModeBanner: false,
      home: ImageLoad(),
    );
  }
}
